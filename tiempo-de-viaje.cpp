#include <iostream>

using namespace std;

int getHours(int minutos) {
    return minutos/60;
}

int getMinutes(int totalMinutos) {
    return totalMinutos%60;
}

int totalTramos() {
    int acumuladorTramos = 0;
    int tramo;
    while(true) {
        cout << "Duracion Tramo" << endl;
        cin >> tramo;

        acumuladorTramos += tramo;

        if(tramo == 0) {
            break;
        }
    }
    return acumuladorTramos;
}

void formatoHora(int minutos) {
    if(getMinutes(minutos) < 10) {
        cout << "Tiempo total de viaje: " << getHours(minutos) << ":0" << getMinutes(minutos) << endl;
    } else {
        cout << "Tiempo total de viaje: " << getHours(minutos) << ":" << getMinutes(minutos) << endl;
    }
}


int main() {
    formatoHora(totalTramos());
    return 0;
}