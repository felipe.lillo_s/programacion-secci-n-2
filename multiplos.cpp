#include <iostream>

using namespace std;

/*
Múltiplos
Escriba un programa que muestre la tabla de multiplicar del 1 al 10 del número ingresado por el usuario:

Ingrese un numero: 9
9 x 1 = 9
9 x 2 = 18
9 x 3 = 27
9 x 4 = 36
9 x 5 = 45
9 x 6 = 54
9 x 7 = 63
9 x 8 = 72
9 x 9 = 81
9 x 10 = 90
 */

void tablaDeMultiplicar(int numero) {
    for(int i = 1; i <= 10; i++) {
        cout << numero
             << " x "
             << i
             << " = "
             << numero * i
             << endl;
    }
}

int main() {
    cout << "Ingrese un numero" << endl;
    int x;
    cin >> x;
    tablaDeMultiplicar(x);
    return 0;
}