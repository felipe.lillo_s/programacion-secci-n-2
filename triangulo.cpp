#include <iostream>

using namespace std;

int main() {
    int angulo1, angulo2, angulo3;

    cout << "Ingrese primer angulo" << endl;
    cin >> angulo1;
    cout << "Ingrese segundo angulo" << endl;
    cin >> angulo2;
    cout << "Ingrese tercer angulo" << endl;
    cin >> angulo3;

    if(angulo1 == 0 || angulo2 == 0 || angulo3 == 0) {
        cout << "Triangulo invalido :(" << endl;
    } else {
        if(angulo1+angulo2+angulo3 == 180) {
            cout << "Triangulo valido :D" << endl;
        } else {
            cout << "Triangulo invalido :(" << endl;
        }
    }

    return 0;
}