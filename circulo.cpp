#include <iostream>
#include <cmath>

using namespace std;
/*
 * Escriba un programa que reciba como entrada el radio de un círculo y entregue como salida su perímetro y su área:
 *
 * Ingrese el radio: 5
 * Perimetro: 31.4
 * Área: 78.5
 *
 */

int main() {
    float radio;
    cout << "Porfavor, ingrese el radio :D" << endl;
    cin  >> radio;

    float perimetro = 2 * radio * 3.14;
    float area = 3.14 * pow(radio, 2);

    cout << "Perímetro: " << perimetro << endl
         << "Área: " << area << endl;

    return 0;
}