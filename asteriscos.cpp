#include <iostream>

using namespace std;

int main() {
    int altura, ancho;
    cout << "Altura" << endl;
    cin >> altura;
    cout << "Ancho" << endl;
    cin >> ancho;

    for(int i = 0; i < altura; i++){
        for(int j = 0; j < ancho; j++) {
            cout << "*";
        }
        cout << endl;
    }

    cout << "Altura" << endl;
    cin >> altura;

    for(int i = 1; i <= altura; i++) {
        for(int j = 1; j <= i; j++) {
            cout << "*";
        }
        cout << endl;
    }

    return 0;
}