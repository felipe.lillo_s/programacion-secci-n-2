#include <iostream>

/*
 * Programe una boletería de cine, que inicialmente pregunte cuantas filas de asientos hay y cuantos asientos por fila hay.
 * Su programa debe preguntar cuantas entradas se quieren comprar y que asiento quiere reservar con esas entradas.
 * No puede sobrevender asientos.
 * Solamente 1 función
 */

using namespace std;

void imprimirSala(int *sala[], int filas, int columnas) {
    cout <<  "        ---------------------------------------------------------------------------------------------------------------------------" << endl;
    cout <<  "        -                                                                                                                         -" << endl;
    cout <<  "        -                                                      PANTALLA                                                           -" << endl;
    cout <<  "        -                                                                                                                         -" << endl;
    cout <<  "        ---------------------------------------------------------------------------------------------------------------------------" << endl;
    for (int i = 0; i < filas; i++) {
        cout
                << "        ---------------------------------------------------------------------------------------------------------------------------"
                << endl << "FILA " << i + 1 << ": ";
        for (int j = 0; j < columnas; j++) {
            cout << "|\t" << "[" << j + 1 << "] " << ((sala[i][j] == 0) ? "Disponible" : "  Ocupado  ") << "\t\t|";
        }
        cout << endl;
    }
    cout <<  "        ---------------------------------------------------------------------------------------------------------------------------" << endl;
}

void inicializarSala(int *sala[], int filas, int columnas) {
    for (int i = 0; i < filas; i++) {
        sala[i] = new int[columnas];
        for (int j = 0; j < columnas; j++) {
            sala[i][j] = 0;
        }
    }
}

bool venderBoleto(int *sala[], int fila, int columna) {
    if(sala[fila-1][columna-1] == 0) {
        sala[fila-1][columna-1] = 1;
        return true;
    }
    return false;
}
void venderBoletos(int *sala[], int filas, int columnas) {
    int fila, columna, entradas;
    cout << "Cuantas entradas desea?" << endl;
    cin >> entradas;
    int i = 0;
    while (i < entradas) {
        imprimirSala(sala, filas, columnas);
        cout << "Asiento entrada " << i + 1 << endl;
        cout << "Ingrese la fila y la columna del asiento que desea" << endl;
        cin >> fila >> columna;
        if (venderBoleto(sala, fila, columna)) {
            cout << "Asiento reservado" << endl;
            i++;
        } else {
            cout << "Lo siento, ese asiento ya está reservado" << endl;
        }
    }
}
void menu(int *sala[], int filas, int columnas) {
    int opcion, entradas;
    while (true) {
        cout << "Bienvenido a la boleteria" << endl << "Ingrese la opción" << endl;
        cout << "1. Comprar boletos" << endl;
        cout << "2. Ver disponibilidad" << endl;
        cout << "0. Salir" << endl;
        cin >> opcion;
        switch(opcion) {
            case 0:
                return;
            case 1:
                venderBoletos(sala, filas, columnas);
                break;
            case 2:
                imprimirSala(sala, filas, columnas);
                break;
            default:
                cout << "Opción invalida :(" << endl;
        }
    }
}

int main() {
    int filas, columnas;
    cout << "UltraCine5D Extra" << endl;
    cout << "Ingrese cantidad de filas" << endl;
    cin >> filas;
    cout << "Ingrese cantidad de asientos por fila" << endl;
    cin >> columnas;
    int *sala[filas];

    inicializarSala(sala, filas, columnas);

    menu(sala, filas, columnas);

    cout << "Adios :) Disfruta tu pelicula" << endl;
    return 0;
}