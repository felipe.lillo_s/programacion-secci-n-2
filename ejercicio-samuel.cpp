#include <iostream>

using namespace std;

// Utilizando la función f(x) = f(x-1)*3 +1 calcule el valor de f(x)
// asignado por un x ingresado por el usuario utilizando ciclos, la condición inicial f(0) = 1

int f(int x) {
    if(x == 0) {
        return 1;
    }
    return f(x-1) * 3 +1;
}

int main() {
    int x;
    cout << "Ingrese el X" << endl;
    cin >> x;

    /*int acumulador = 1;
    for(int i = 1; i <= x; i++) {
        acumulador *= 3;
        acumulador += 1
    }
    cout << "F(" << x << ") = " << acumulador << endl;
     */
    cout << "F(" << x << ") = " << f(x) << endl;

    return 0;
}
