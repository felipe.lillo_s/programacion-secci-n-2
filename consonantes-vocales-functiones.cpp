#include <iostream>

using namespace std;

bool estaEnAlfabeto(char entrada) {
    if ((entrada >= 'a' && entrada <= 'z') || (entrada >= 'A' && entrada <= 'Z')) {
        return true;
    }
    return false;
}

bool esVocal(char entrada) {
    if (!estaEnAlfabeto(entrada)) { return false; }
    if (entrada == 'a' || entrada == 'A') {
        return true;
    } else if (entrada == 'e' || entrada == 'E') {
        return true;
    } else if (entrada == 'i' || entrada == 'I') {
        return true;
    } else if (entrada == 'o' || entrada == 'O') {
        return true;
    } else if (entrada == 'u' || entrada == 'U') {
        return true;
    }
    return false;
}

bool esConsonante(char entrada) {
    if (estaEnAlfabeto(entrada) && !esVocal(entrada)) {
        return true;
    }
    return false;
}

bool esMayuscula(char entrada) {
    if (entrada >= 'A' && entrada <= 'Z') {
        return true;
    }
    return false;
}

bool esMinuscula(char entrada) {
    if (entrada >= 'a' && entrada <= 'z') {
        return true;
    }
    return false;
}

int main() {
    char c;

    cout << "Ingrese un caracter" << endl;
    cin >> c;

    if (esVocal(c)) {
        cout << c << " es vocal" << endl;
        if (esMayuscula(c)) {
            cout << " es mayuscula " << endl;
        }
        if (esMinuscula(c)) {
            cout << " es minuscula " << endl;
        }
    } else if (esConsonante(c)) {
        cout << c << " es consonante" << endl;
        if (esMayuscula(c)) {
            cout << " es mayuscula " << endl;
        }
        if (esMinuscula(c)) {
            cout << " es minuscula " << endl;
        }
    } else {
        cout << c << " es un simbolo" << endl;
    }
    return 0;
}