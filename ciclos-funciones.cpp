#include <iostream>

using namespace std;

// Cree una función que determine si un numero es primo o no.
// En el main, pida un numero por teclado e imprima si el numero es primo o no.

void esPrimo(int numero) {
    for(int i = 2; i < numero; i++) { // i = i + 1
        if(numero%i == 0) {
            cout << "no es primo" << endl;
            return;
        }
    }
    cout << "es primo" << endl;
}

int main() {
    int x;

    cout << "ingrese un numero" << endl;
    cin >> x;

    esPrimo(x);

    return 0;
}