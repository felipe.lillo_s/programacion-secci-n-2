#include <iostream>

using namespace std;

int main() {
    char c;

    cout << "Ingrese un caracter" << endl;

    cin >> c;

    if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
        cout << c << " es parte del alfabeto" << endl;
    } else if(c >= '0' && c <= '9') {
        cout << c << " es un digito" << endl;
    } else {
        cout << c << " es un símbolo" << endl;
    }
    return 0;
}