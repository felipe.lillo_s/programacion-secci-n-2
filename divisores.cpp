#include <iostream>

using namespace std;

void divisores(int x) {
    for(int i = 1; i <= x; i++) {
        if(x % i == 0) {
            cout << i << " ";
        }
    }
}

int main() {
    int numero;
    cout << "Ingrese numero" << endl;
    cin >> numero;

    divisores(numero);
    cout << endl;

    return 0;
}