#include <iostream>

using namespace std;

/*
 * Escriba un programa que determine si el número entero ingresado por el usuario es par o no.

Ingrese un número: 4
Su número es par
Ingrese un número: 3
Su número es impar
 */
int main() {

    int numero;

    cout << "Ingrese un número" << endl;
    cin >> numero;

    if(numero % 2 == 0) {
        cout << "Su número es par" << endl;
    } else {
        cout << "Su número es impar" << endl;
    }

    // :D
    return 0;
}