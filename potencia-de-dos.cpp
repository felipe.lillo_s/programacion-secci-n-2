#include <iostream>

using namespace std;

int potencia(int base, int exponente) {
    int acumulador = 1;
    for(int i = 1; i <= exponente; i++) {
        acumulador = acumulador * base; // 125
    }
    return acumulador;
}

int main() {
    cout << "Ingrese num" << endl;
    int num;
    cin >> num;
    for(int i = 0; i <= num; i++) { // 0,1,2...10
        cout << potencia(2, i) << " ";
    }
    cout << endl;
    return 0;
}