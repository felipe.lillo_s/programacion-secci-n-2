//
// Created by Maximiliano Vega on 19-05-20.
//

#include <iostream>

using namespace std;

int funcion(int *arreglo[], int largo, int ancho) {
    int acumulador = 0;
    for(int i = 0; i < largo; i++) {
        for(int j = 0; j < ancho; j++) {
            acumulador += arreglo[i][j];
        }
    }
    return acumulador;
}

int main() {
    int *arreglo[2];
    for(int i = 0; i < 2; i++) {
        arreglo[i] = new int[2];
        for(int j = 0; j < 2; j++) {
            cin >> arreglo[i][j];
        }
    }
    cout << funcion(arreglo, 2, 2) << endl;

}