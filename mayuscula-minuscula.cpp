#include <iostream>

using namespace std;

int main() {
    char c;

    cout << "Ingrese un caracter" << endl;

    cin >> c;

    if (c >= 'a' && c <= 'z') {
        cout << c << " es minuscula" << endl;
    } else if (c >= 'A' && c <= 'Z') {
        cout << c << " es mayuscula" << endl;
    } else {
        cout << c << " es otra cosa" << endl;
    }
    return 0;
}