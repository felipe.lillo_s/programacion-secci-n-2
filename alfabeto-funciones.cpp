#include <iostream>

using namespace std;

bool estaEnAlfabeto(char entrada) {
    if ((entrada >= 'a' && entrada <= 'z') || (entrada >= 'A' && entrada <= 'Z')) {
        return true;
    }
    return false;
}

int main() {

    char c;

    cout << "Escribe un caracter" << endl;

    cin >> c;

    if (estaEnAlfabeto(c)) {
        cout << c << " esta dentro del alfabeto" << endl;
    } else {
        cout << c << " nup" << endl;
    }

    return 0;
}
