#include <iostream>

using namespace std;

int sumaEntreDosNumeros(int a, int b) {
    int acumulador = 0;
    for(int i = a+1; i < b; i++) {
        acumulador += i; // acumulador = acumulador +i
    }
    return acumulador;
}

int main() {
    int numero1, numero2;

    cout << "Ingrese numero" << endl;

    cin >> numero1;

    cout << "Ingrese numero" << endl;

    cin >> numero2;

    if(numero1 > numero2) {
        cout << "Not today" << endl;
    } else {
        cout << "La suma es " << sumaEntreDosNumeros(numero1, numero2) << endl;
    }

    return 0;
}