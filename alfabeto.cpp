#include <iostream>

using namespace std;

int main() {
    char c;

    cout << "Ingrese un caracter" << endl;
    cin >> c;

    if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
        cout << c << " esta dentro del alfabeto" << endl;
    } else {
        cout << "Nup" << endl;
    }

    return 0;
}