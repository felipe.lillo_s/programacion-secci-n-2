#include <iostream>

using namespace std;

int main() {
    for(int i = 1; i <= 10; i++) { // 1,2,3,4...10
        for(int j = 1; j <= 10; j++) { // 1,2,3,4...10
            cout << j*i << "\t\t";
        }
        cout << endl;
    }
    return 0;
}
