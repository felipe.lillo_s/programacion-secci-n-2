/*Transmisión de datos
        En varios sistemas de comunicaciones digitales los datos viajan de manera serial (es decir, uno tras otro), y en bloques de una cantidad fija de bits (valores 0 o 1). La transmisión física de los datos no conoce de esta separación por bloques, y por lo tanto es necesario que haya programas que separen y organicen los datos recibidos.

Los datos transmitidos los representaremos como arreglos cuyos valores son ceros y unos.

Una secuencia de bits puede interpretarse como un número decimal. Cada bit está asociado a una potencia de dos, partiendo desde el último bit. Por ejemplo, la secuencia 01001 representa al número decimal 9, ya que:

0⋅2^4+1⋅2^3+0⋅2^2+0⋅2^1+1⋅2^0=9

*/
#include <iostream>
#include <cstring>
#include <cmath>

using namespace std;


int binario_a_decimal(string trama) {
    int j = 0;
    int acumulador = 0;

    for (int i = trama.size() - 1; i >= 0; i--) {
        if (trama[j] == '1') {
            acumulador += pow(2, i);
        }
        //acumulador += (trama[j] - '0') * pow(2, i);
        j++;
    }
    return acumulador;
}

bool bloque_valido(string trama) {
    if(trama.size() % 4 == 0) {
        return true;
    }
    return false;
}

int *decodificar_bloques(string trama) {
    if (trama.size() < 4) {
        return -1;
    }
    bool tramaMala = false;
    int largoRetorno = trama.size()/4;
    if(trama.size() % 4 != 0) {
        trama = trama.substr(0, trama.size() - trama.size()%4);
        tramaMala = true;
        largoRetorno += 1;
    }
    int *retorno = new int[largoRetorno];
    int j = 0;
    for(int i = 0; i <= trama.size(); i += 4) {
        string corte = trama.substr(i, 4);
        retorno[j] = binario_a_decimal(corte);
        j++;
    }
    if(tramaMala) {
        retorno[j+1] = -1
    }
    return retorno;
}

int main() {
    string trama;
    cout << "Ingrese trama de datos" << endl;
    getline(cin, trama);

    cout << "Valor decimal de la trama " << trama << ": " << binario_a_decimal(trama) << endl;

    return 0;
}