#include <iostream>

using namespace std;

int main() {
    char c;

    cout << "Ingrese una vocal o consonante" << endl;
    cin >> c;

    if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
        if (c == 'a' || c == 'A' || c == 'e' || c == 'E' || c == 'i' || c == 'I' || c == 'o' || c == 'O' || c == 'u' || c == 'U') {
            cout << c << " es una vocal." << endl;
        } else {
            cout << c << " es una consonante." << endl;
        }
    } else {
        cout << c << " no es ni consonante ni vocal" << endl;
    }
    return 0;
}